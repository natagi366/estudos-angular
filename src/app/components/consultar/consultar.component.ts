import { Component, OnInit } from '@angular/core';
import { Produtos } from '../../models/produtos';
import { ProdutosService } from '../../services/produtos/produtos.service';

@Component({
  selector: 'app-consultar',
  templateUrl: './consultar.component.html',
  styleUrls: ['./consultar.component.css']
})
export class ConsultarComponent implements OnInit {

  produtos: Produtos[] = [];
  displayedColumns: string[] = ['id', 'nome', 'descricao', 'preco'];

  constructor(private produtosService: ProdutosService) { }

  ngOnInit(): void {
    this.produtosService.getAll().subscribe((produtos: Produtos[]) => {
      this.produtos = produtos;
    });
  }

}
