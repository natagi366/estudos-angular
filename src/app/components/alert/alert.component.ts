import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AlertService } from '../../services/alert/alert.service';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit {

  constructor(private snackBar: MatSnackBar, private alertService: AlertService) {}

  ngOnInit(): void {
    this.alertService.recebeMensagem().subscribe((sMensagem) => {
      this.openSnackBar(sMensagem);
    });
  }

  openSnackBar(sMensagem: string) {
    this.snackBar.open(sMensagem, "", {
      duration: 5 * 1000,
    });
  }

}
