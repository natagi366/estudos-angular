import { HomeComponent } from './components/home/home.component';
import { ConsultarComponent } from './components/consultar/consultar.component';

const routes = [
    { path: '', component: HomeComponent, label: "Home", icon: "home" },
    { path: 'consultar', component: ConsultarComponent, label: "Consultar", icon: "search" },
];

export default routes;