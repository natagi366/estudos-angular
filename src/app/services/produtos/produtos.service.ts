import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Produtos } from '../../models/produtos';
import { environment } from '../../../environments/environment';
import { ErrorService } from '../error/error.service';

@Injectable({
  providedIn: 'root'
})

export class ProdutosService {

  private endpoint = `${environment.api}/produtos`;

  constructor(private httpClient: HttpClient, private errorService: ErrorService) {}

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }

  // Retorna todos os produtos
  getAll(): Observable<Produtos[]> {
    return this.httpClient.get<Produtos[]>(this.endpoint)
      .pipe(
        retry(2),// caso aconteça algum erro na nossa requisição, tenta reenviar mais duas vezes
        catchError(err => this.errorService.handleError(err)))
  }
}
