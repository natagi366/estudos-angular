import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { Subject } from 'rxjs/internal/Subject';

@Injectable({
  providedIn: 'root'
})

export class AlertService {

  private subject = new Subject<string>();

  enviaMensagem(texto: string) {
    this.subject.next(texto);
  }

  recebeMensagem(): Observable<string> {
    return this.subject.asObservable();
  }
}
