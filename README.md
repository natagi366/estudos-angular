# Estudos Angular

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.5.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.


## Configuração

Deve entrar no diretório do projeto e seguir os seguintes passos:

1. Executar o comando **npm install** para instalar as dependências

2. Executar o comando **json-server --watch server/db.json --host 0.0.0.0** para subir a API

3. Executar o comando **ng serve --host 0.0.0.0 --disable-host-check** para subir o servidor de desenvolvimento

4. Alterar o arquivo **src/environments/environment.ts** na linha **7** para o **IP** da sua maquina

